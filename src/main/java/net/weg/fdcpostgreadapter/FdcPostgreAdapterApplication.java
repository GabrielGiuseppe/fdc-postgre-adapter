package net.weg.fdcpostgreadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FdcPostgreAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(FdcPostgreAdapterApplication.class, args);
	}

}
