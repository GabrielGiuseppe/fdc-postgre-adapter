<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.2.0</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>net.weg</groupId>
	<artifactId>fdc-postgre-adapter</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>fdc-postgre-adapter</name>
	<description>Adaptador para processamento de persistencia em base PostgreSQL</description>
	<properties>
		<java.version>21</java.version>
		<ecs-logging-java.version>1.0.1</ecs-logging-java.version>
		<fixture-factory-version>3.1.0</fixture-factory-version>
		<powermock-version>2.0.7</powermock-version>
		<springfox-swagger-ui-version>2.10.5</springfox-swagger-ui-version>
		<springfox-swagger2-version>2.9.2</springfox-swagger2-version>
		<postgresql-version>42.6.0</postgresql-version>
		<HikariCP-version>4.0.3</HikariCP-version>
		<jjwt-version>0.9.1</jjwt-version>
		<feign-version>8.18.0</feign-version>
		<squiggly-filter-jackson-version>1.3.18</squiggly-filter-jackson-version>
		<commons-lang3-version>3.12.0</commons-lang3-version>
		<apache-poi-version>5.0.0</apache-poi-version>
		<commons-csv-version>1.8</commons-csv-version>
		<gson-version>2.9.1</gson-version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.apache.tomcat</groupId>
					<artifactId>tomcat-jdbc</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-tomcat</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jetty</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>

		<dependency>
			<groupId>br.com.six2six</groupId>
			<artifactId>fixture-factory</artifactId>
			<version>${fixture-factory-version}</version>
		</dependency>

		<dependency>
			<groupId>org.powermock</groupId>
			<artifactId>powermock-module-junit4</artifactId>
			<version>${powermock-version}</version>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.mockito</groupId>
					<artifactId>mockito-core</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.powermock</groupId>
			<artifactId>powermock-api-mockito2</artifactId>
			<version>${powermock-version}</version>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.mockito</groupId>
					<artifactId>mockito-core</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Swagger -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${springfox-swagger-ui-version}</version>
			<scope>compile</scope>
		</dependency>

		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${springfox-swagger2-version}</version>
			<scope>compile</scope>
		</dependency>

		<!-- PostgresSql -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>${postgresql-version}</version>
		</dependency>

		<!-- Flyway -->
		<dependency>
			<groupId>org.flywaydb</groupId>
			<artifactId>flyway-core</artifactId>
		</dependency>

		<!-- Hikari -->
		<dependency>
			<groupId>com.zaxxer</groupId>
			<artifactId>HikariCP</artifactId>
			<version>${HikariCP-version}</version>
		</dependency>

		<!-- Lombok -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>

		<!-- JWT -->
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>${jjwt-version}</version>
		</dependency>

		<!-- Feign -->
		<dependency>
			<groupId>com.netflix.feign</groupId>
			<artifactId>feign-core</artifactId>
			<version>${feign-version}</version>
		</dependency>

		<dependency>
			<groupId>com.netflix.feign</groupId>
			<artifactId>feign-jackson</artifactId>
			<version>${feign-version}</version>
		</dependency>

		<dependency>
			<groupId>com.netflix.feign</groupId>
			<artifactId>feign-slf4j</artifactId>
			<version>${feign-version}</version>
		</dependency>
		<dependency>
			<groupId>io.micrometer</groupId>
			<artifactId>micrometer-registry-prometheus</artifactId>
		</dependency>
		<dependency>
			<groupId>co.elastic.logging</groupId>
			<artifactId>logback-ecs-encoder</artifactId>
			<version>${ecs-logging-java.version}</version>
		</dependency>

		<!-- squiggly-json-filter -->
		<dependency>
			<groupId>com.github.bohnman</groupId>
			<artifactId>squiggly-filter-jackson</artifactId>
			<version>${squiggly-filter-jackson-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${commons-lang3-version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.dataformat</groupId>
			<artifactId>jackson-dataformat-xml</artifactId>
		</dependency>

		<!-- apache-poi -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
			<version>${apache-poi-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>${apache-poi-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-csv</artifactId>
			<version>${commons-csv-version}</version>
		</dependency>

		<!-- unit tests -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>${gson-version}</version>
		</dependency>
		<dependency>
			<groupId>org.junit.vintage</groupId>
			<artifactId>junit-vintage-engine</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.hamcrest</groupId>
					<artifactId>hamcrest-core</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
